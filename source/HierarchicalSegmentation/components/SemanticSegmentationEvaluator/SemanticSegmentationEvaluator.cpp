/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    HierarchicalSegmentation::ArmarXObjects::SemanticSegmentationEvaluator
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#include "SemanticSegmentationEvaluator.h"


using namespace armarx;


SemanticSegmentationEvaluator::SemanticSegmentationEvaluator()
{
    maxDifference = 1;
    missedKeyframesPenalty = 5;
    falsePositiveKeyframesPenalty = 5;
}


void SemanticSegmentationEvaluator::onInitComponent()
{

}


void SemanticSegmentationEvaluator::onConnectComponent()
{

}


void SemanticSegmentationEvaluator::onDisconnectComponent()
{

}


void SemanticSegmentationEvaluator::onExitComponent()
{

}

PropertyDefinitionsPtr SemanticSegmentationEvaluator::createPropertyDefinitions()
{
    return PropertyDefinitionsPtr(new SemanticSegmentationEvaluatorPropertyDefinitions(
                                      getConfigIdentifier()));
}


double SemanticSegmentationEvaluator::calculateMetric(std::vector<double> testKeyframes, const std::vector<double> &groundTruthKeyFrames) const
{
    double error = 0;
    int unmatchedKeyFrames = testKeyframes.size();
    for(auto itGT = groundTruthKeyFrames.begin(); itGT != groundTruthKeyFrames.end(); itGT++)
    {

        double bestDistance = std::numeric_limits<double>::max();
        auto bestKeyFrame = testKeyframes.end();
        for(auto it = testKeyframes.begin(); it != testKeyframes.end(); it++)
        {
            double distance = fabs(*it-*itGT);
            if(distance < bestDistance)
            {
                bestDistance = distance;
                bestKeyFrame = it;
            }
        }
        if(bestDistance <= maxDifference)
        {
            error += bestDistance*bestDistance;
            testKeyframes.erase(bestKeyFrame);
            unmatchedKeyFrames--;
        }
        else
        {
            error += missedKeyframesPenalty;
        }
    }
    error += falsePositiveKeyframesPenalty * unmatchedKeyFrames;
    return error/groundTruthKeyFrames.size();
}


/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    HierarchicalSegmentation::ArmarXObjects::SemanticSegmentationEvaluator
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_COMPONENT_HierarchicalSegmentation_SemanticSegmentationEvaluator_H
#define _ARMARX_COMPONENT_HierarchicalSegmentation_SemanticSegmentationEvaluator_H


#include <ArmarXCore/core/Component.h>

namespace armarx
{
    /**
     * @class SemanticSegmentationEvaluatorPropertyDefinitions
     * @brief
     * @ingroup Components
     */
    class SemanticSegmentationEvaluatorPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        SemanticSegmentationEvaluatorPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            //defineRequiredProperty<std::string>("PropertyName", "Description");
            //defineOptionalProperty<std::string>("PropertyName", "DefaultValue", "Description");
        }
    };

    /**
     * @class SemanticSegmentationEvaluator
     * @ingroup Components
     * @brief A brief description
     *
     *
     * Detailed Description
     */
    class SemanticSegmentationEvaluator :
        virtual public armarx::Component
    {
    public:
        SemanticSegmentationEvaluator();
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        virtual std::string getDefaultName() const
        {
            return "SemanticSegmentationEvaluator";
        }

        double calculateMetric(std::vector<double> testKeyframes, const std::vector<double> &groundTruthKeyFrames) const;


    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        virtual void onInitComponent();

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        virtual void onConnectComponent();

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        virtual void onDisconnectComponent();

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        virtual void onExitComponent();

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        virtual armarx::PropertyDefinitionsPtr createPropertyDefinitions();


        double maxDifference;
        double missedKeyframesPenalty;
        double falsePositiveKeyframesPenalty;
    };
}

#endif

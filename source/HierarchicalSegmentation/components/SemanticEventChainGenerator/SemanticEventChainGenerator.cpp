/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    HierarchicalSegmentation::ArmarXObjects::SemanticEventChainGenerator
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#include "SemanticEventChainGenerator.h"
#include <HierarchicalSegmentation/libraries/SemanticEventChains/AutomaticActionSegmentation.h>
#include "ViconCSVParser.h"

#include <ArmarXCore/core/system/ArmarXDataPath.h>

#include <MemoryX/libraries/memorytypes/entity/SEC/SECRelation.h>
#include <MemoryX/core/MemoryXCoreObjectFactories.h>
#include <MemoryX/libraries/memorytypes/MemoryXTypesObjectFactories.h>
#include <MemoryX/libraries/memorytypes/segment/ObjectClassMemorySegment.h>




using namespace armarx;
using namespace memoryx;
using namespace hierarchicalsegmentation;

void SemanticEventChainGenerator::onInitComponent()
{
    usingProxy("PriorKnowledge");
}


void SemanticEventChainGenerator::onConnectComponent()
{
    priorKnowledge = getProxy<PriorKnowledgeInterfacePrx>("PriorKnowledge");
    objectClassSegment = priorKnowledge->getObjectClassesSegment();



//    ViconCSVParser csvParser;

//    std::string absolutePath;
//    ArmarXDataPath::getAbsolutePath(getProperty<std::string>("ViconCSVFile").getValue(), absolutePath);
//    csvParser.readFromCSVFile(absolutePath, false);
//    calculateSEC(csvParser.getLines());
}


void SemanticEventChainGenerator::onDisconnectComponent()
{

}


void SemanticEventChainGenerator::onExitComponent()
{

}

PropertyDefinitionsPtr SemanticEventChainGenerator::createPropertyDefinitions()
{
    return PropertyDefinitionsPtr(new SemanticEventChainGeneratorPropertyDefinitions(
                                      getConfigIdentifier()));
}

ObjectKeyframes SemanticEventChainGenerator::calcKeyframes(const std::string &MMMFilePath, const Ice::Current &)
{
    throw NotImplementedYetException();
}

Keyframes SemanticEventChainGenerator::getXPositions(const std::string &, const std::string &, const Ice::Current &)
{
    throw NotImplementedYetException();
}

ObjectKeyframes SemanticEventChainGenerator::calcKeyframesWithConfig(const std::string &, const DoubleList &, const Ice::Current &)
{
    throw NotImplementedYetException();
}


SECKeyFrameMap armarx::SemanticEventChainGenerator::calculateSEC(const StringList & viconCSVString, const Ice::Current &c)
{
    ObjectClassList allObjects = objectClassSegment->getClassWithSubclasses("all");
    ObjectClassList allHands = objectClassSegment->getClassWithSubclasses("hand");
    ObjectClassList allContainer = objectClassSegment->getClassWithSubclasses("container");

    allObjects.insert(allObjects.end(), allHands.begin(), allHands.end());
    ARMARX_VERBOSE << "Objects: " << memoryx::ObjectClassMemorySegment::ObjectsToString(allObjects);
    ARMARX_VERBOSE << "allContainer: " << memoryx::ObjectClassMemorySegment::ObjectsToString(allContainer);

    ViconCSVParser csvParser;


    csvParser.readFromString(viconCSVString, 5);
    std::vector<std::vector<std::vector<double> > > positions;
    csvParser.convertToCartesianVectors(positions, 2);

    std::map<std::string, std::vector<int> > markerGroups = csvParser.getMarkerGroups();


    AutomaticActionSegmentation aas(150,100,100,50,100);
    aas.setObjects(allObjects);
//    aas.setMergeKeyFrames(false);
    aas.calc(positions, markerGroups, false);
    ARMARX_INFO << "XML:" << aas.getXMLString();
//    aas.plot(csvParser.getColumn(0), AutomaticActionSegmentation::eSVG);
    ARMARX_INFO << "keyframe count:  " << aas.getKeyFrames().size();
    return aas.getKeyFrames();
}

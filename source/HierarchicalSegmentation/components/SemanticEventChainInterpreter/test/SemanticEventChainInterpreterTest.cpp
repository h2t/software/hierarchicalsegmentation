/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    HierarchicalSegmentation::ArmarXObjects::SemanticEventChainInterpreter
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE HierarchicalSegmentation::ArmarXObjects::SemanticEventChainInterpreter

#define ARMARX_BOOST_TEST
#include <HierarchicalSegmentation/Test.h>
#include <ArmarXCore/core/test/IceTestHelper.h>
#include <MemoryX/libraries/memorytypes/entity/test/SECEnvironment.h>
#include <MemoryX/libraries/memorytypes/entity/ObjectClass.h>


#include <iostream>
#include <HierarchicalSegmentation/components/SemanticEventChainInterpreter/SemanticEventChainInterpreter.h>
#include <MemoryX/libraries/memorytypes/entity/Oac.h>
#include <MemoryX/libraries/memorytypes/entity/SEC/SECRelation.h>
#include <MemoryX/libraries/memorytypes/entity/SEC/SECKeyFrame.h>
#include <MemoryX/libraries/memorytypes/entity/SEC/SECObjectRelations.h>
#include <MemoryX/libraries/memorytypes/entity/OacPredictionFunction.h>
#include <HierarchicalSegmentation/components/SemanticEventChainGenerator/SemanticEventChainGenerator.h>
using namespace memoryx;
using namespace armarx;

class OACLibrary
{
public:
    OACLibrary()
    {

        ObjectClassBasePtr any =  new ObjectClass();
        any->setName("any");

        ObjectClassBasePtr hand =  new ObjectClass();
        hand->setName("hand");
        hand->setParentClass("any");

        ObjectClassBasePtr cupblue =  new ObjectClass();
        cupblue->setName("cupblue");
        cupblue->setParentClass("any");

        ObjectClassBasePtr bowl =  new ObjectClass();
        bowl->setName("bowl");
        bowl->setParentClass("any");



        if(!any)
            ARMARX_WARNING_S << "'any' object not found in mongo";
        if(!hand)
            ARMARX_WARNING_S << "'hand' object not found in mongo";
        if(!cupblue)
            ARMARX_WARNING_S << "'cupblue' object not found in mongo";
        if(!bowl)
            ARMARX_WARNING_S << "'bowl' object not found in mongo";

        listcup.push_back(cupblue);
        listbowl.push_back(bowl);
        listhand.push_back(hand);
        listany.push_back(any);


        SECRelationBasePtr relationHandEmpty = new Relations::NoConnectionRelation(listhand, listany);
        SECObjectRelationsBasePtr preconditions = new SECObjectRelations();
        preconditions->addRelation(relationHandEmpty);

        SECRelationBasePtr relationObjInHand = new Relations::TouchingRelation(listhand, listany);
        SECObjectRelationsBasePtr effects = new SECObjectRelations();
        effects->addRelation(relationObjInHand);


        OacPredictionFunctionBasePtr pred = new OacPredictionFunction();
        pred->setSECPreconditions(preconditions);
        pred->setSECEffects(effects);

        SECRelationPair sideConstraint;
        sideConstraint.relation1 = relationHandEmpty;
        sideConstraint.relation2 = relationObjInHand;
        SECRelationPairList sideConstraints;
        sideConstraints.push_back(sideConstraint);
        pred->setSECSideConstraints(sideConstraints);

        oacGrasp = new Oac("grasp");
        oacGrasp->setPredictionFunction(pred);


    }

    void addToMongo(SECEnvironmentPtr env)
    {
        assureObjectinMongo(env, *listcup.begin());
        assureObjectinMongo(env, *listbowl.begin());
        assureObjectinMongo(env, *listhand.begin());
        assureObjectinMongo(env, *listany.begin());

        if(!env->ltm->getOacSegment()->getOacByName("grasp"))
        {
            env->ltm->getOacSegment()->addEntity(oacGrasp);
        }
    }


    void assureObjectinMongo(SECEnvironmentPtr env, ObjectClassBasePtr obj)
    {
        std::string objName = obj->getName();
        if(!env->priorKnowledgePrx->getObjectClassesSegment()->getObjectClassByName(objName))
            env->priorKnowledgePrx->getObjectClassesSegment()->addEntity(obj);
    }

    ObjectClassList listcup;
    ObjectClassList listbowl;
    ObjectClassList listhand;
    ObjectClassList listany;

    OacPtr oacGrasp;
};

BOOST_AUTO_TEST_CASE(SemanticEventChainInterpreterTestMatchSuccess)
{
    ARMARX_IMPORTANT_S << "SemanticEventChainInterpreterTestMatchSuccess";
    armarx::SemanticEventChainInterpreter instance;
    OACLibrary oacLib;
//    IceTestHelper iceHelper;
//    SECEnvironment env("SECTest");


//    PersistentObjectClassSegmentBasePrx objClassSeg = env->priorKnowledgePrx->getObjectClassesSegment();
//    ObjectClassBasePtr any =  objClassSeg->getObjectClassByName("any");
//    ObjectClassBasePtr hand =  objClassSeg->getObjectClassByName("hand");
//    ObjectClassBasePtr cupblue =  objClassSeg->getObjectClassByName("cupblue");
//    ObjectClassBasePtr bowl =  objClassSeg->getObjectClassByName("bowl");


    SECRelationBasePtr relationCupNotInHand = new Relations::NoConnectionRelation(oacLib.listhand, oacLib.listcup);
    SECRelationBasePtr relationBowlNotInHand = new Relations::NoConnectionRelation(oacLib.listhand, oacLib.listbowl);
    SECObjectRelationsBasePtr worldStateBefore = new SECObjectRelations();
    worldStateBefore->addRelation(relationCupNotInHand);
    worldStateBefore->addRelation(relationBowlNotInHand);

    SECRelationBasePtr relationCupInHand = new Relations::TouchingRelation(oacLib.listhand, oacLib.listcup);
    SECObjectRelationsBasePtr worldStateAfter = new SECObjectRelations();
    worldStateAfter->addRelation(relationCupInHand);
    worldStateAfter->addRelation(relationBowlNotInHand);



    SemanticEventChainInterpreterPtr interpreter = armarx::Component::create<SemanticEventChainInterpreter>();

    ObjectClassList temp;
    BOOST_CHECK_EQUAL(interpreter->matchOAC(oacLib.oacGrasp, worldStateBefore, worldStateAfter, temp), 1.0f);
}


BOOST_AUTO_TEST_CASE(SemanticEventChainInterpreterTestFailMatch)
{
    ARMARX_IMPORTANT_S << "SemanticEventChainInterpreterTestFailMatch";
    armarx::SemanticEventChainInterpreter instance;
    OACLibrary oacLib;

    SECRelationBasePtr relationCupNotInHand = new Relations::NoConnectionRelation(oacLib.listhand, oacLib.listcup);
    SECRelationBasePtr relationBowlInHand = new Relations::TouchingRelation(oacLib.listhand, oacLib.listbowl);
//    SECRelationBasePtr relationBowlNotInHand = new Relations::NoConnectionRelation(listhand, listbowl);
    SECObjectRelationsBasePtr worldStateBefore = new SECObjectRelations();
    worldStateBefore->addRelation(relationCupNotInHand);
    worldStateBefore->addRelation(relationBowlInHand);

    SECRelationBasePtr relationCupInHand = new Relations::TouchingRelation(oacLib.listhand, oacLib.listcup);
    SECObjectRelationsBasePtr worldStateAfter = new SECObjectRelations();
    worldStateAfter->addRelation(relationCupNotInHand);
    worldStateAfter->addRelation(relationBowlInHand);



    SemanticEventChainInterpreterPtr interpreter = armarx::Component::create<SemanticEventChainInterpreter>();

    ObjectClassList temp;
    BOOST_CHECK(interpreter->matchOAC(oacLib.oacGrasp, worldStateBefore, worldStateAfter, temp) <  1.0f);
}

BOOST_AUTO_TEST_CASE(SemanticEventChainInterpreterMongoTest)
{

    ARMARX_IMPORTANT_S << "SemanticEventChainInterpreterMongoTest";
    OACLibrary oacLib;
//    IceTestHelper iceHelper;
    SECEnvironmentPtr env;
    try
    {
        env.reset(new SECEnvironment("SECTest"));
    }
    catch(std::exception &e)
    {
        ARMARX_WARNING_S << "Setting up environment failed - skipping test: " << e.what();
        return;
    }
    EntityBasePtr oac1 = env->ltm->getOacSegment()->getEntityByName("oac1");
    if(oac1)
    {
            env->ltm->getOacSegment()->removeEntity(oac1->getId());
    }
    oacLib.addToMongo(env);
    ARMARX_IMPORTANT_S << "creating SEC components";
    hierarchicalsegmentation::SemanticEventChainGeneratorInterfacePrx generator = env->manager->createComponentAndRun<SemanticEventChainGenerator, hierarchicalsegmentation::SemanticEventChainGeneratorInterfacePrx>("MemoryX", "SemanticEventChainGenerator");

    hierarchicalsegmentation::SemanticEventChainInterpreterInterfacePrx interpreter = env->manager->createComponentAndRun<SemanticEventChainInterpreter, hierarchicalsegmentation::SemanticEventChainInterpreterInterfacePrx>("MemoryX", "SemanticEventChainInterpreter");

//    PersistentObjectClassSegmentBasePrx objClassSeg = env->priorKnowledgePrx->getObjectClassesSegment();
//    ObjectClassBasePtr any =  objClassSeg->getObjectClassByName("any");
//    ObjectClassBasePtr hand =  objClassSeg->getObjectClassByName("hand");
//    ObjectClassBasePtr cupblue =  objClassSeg->getObjectClassByName("cupblue");
//    ObjectClassBasePtr bowl =  objClassSeg->getObjectClassByName("bowl");

    ARMARX_IMPORTANT_S << "creating SEC Relations";
    SECRelationBasePtr relationCupNotInHand = new Relations::NoConnectionRelation(oacLib.listhand, oacLib.listcup);
    SECRelationBasePtr relationBowlNotInHand = new Relations::NoConnectionRelation(oacLib.listhand, oacLib.listbowl);
    SECObjectRelationsBasePtr worldStateBefore = new SECObjectRelations();
    worldStateBefore->addRelation(relationCupNotInHand);
    worldStateBefore->addRelation(relationBowlNotInHand);

    SECRelationBasePtr relationCupInHand = new Relations::TouchingRelation(oacLib.listhand, oacLib.listcup);
    SECObjectRelationsBasePtr worldStateAfter = new SECObjectRelations();
    worldStateAfter->addRelation(relationCupInHand);
    worldStateAfter->addRelation(relationBowlNotInHand);


    SECKeyFrameMap keyFrames;
    keyFrames[0] = new SECKeyFrame(0, worldStateBefore);
    keyFrames[1] = new SECKeyFrame(1, worldStateAfter);

    SECMotionSegmentList segments = interpreter->findOACSequence(keyFrames);
    ARMARX_INFO_S << "Winner OAC: " << segments.begin()->oacs.at(0)->getName();
    BOOST_CHECK_EQUAL(segments.begin()->oacs.at(0)->getName(), "grasp");
}


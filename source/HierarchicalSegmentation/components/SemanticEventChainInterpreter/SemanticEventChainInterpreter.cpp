/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    HierarchicalSegmentation::ArmarXObjects::SemanticEventChainInterpreter
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#include "SemanticEventChainInterpreter.h"

#include <fstream>


#include <MemoryX/libraries/memorytypes/entity/SEC/SECKeyFrame.h>
#include <MemoryX/libraries/memorytypes/MemoryXTypesObjectFactories.h>
#include <MemoryX/core/MemoryXCoreObjectFactories.h>
#include <MemoryX/libraries/memorytypes/segment/ObjectClassMemorySegment.h>

#include <ArmarXCore/core/system/ArmarXDataPath.h>

#include <HierarchicalSegmentation/components/SemanticEventChainGenerator/ViconCSVParser.h>

#include <MMM/Motion/Motion.h>
#include <MMM/Motion/MotionReaderXML.h>

using namespace armarx;
using namespace memoryx;




void SemanticEventChainInterpreter::onInitComponent()
{

    OACMatchingThreshold = getProperty<float>("OACMatchingThreshold").getValue();
    usingProxy("PriorKnowledge");
    usingProxy("LongtermMemory");
    usingProxy(getProperty<std::string>("SECGeneratorName").getValue());
//    usingProxy("SECtoPEMConverter");
}


void SemanticEventChainInterpreter::onConnectComponent()
{
    priorKnowledge = getProxy<PriorKnowledgeInterfacePrx>("PriorKnowledge");
    longtermMemory = getProxy<LongtermMemoryInterfacePrx>("LongtermMemory");
    objClassSegment = priorKnowledge->getObjectClassesSegment();
    secGenPrx = getProxy<hierarchicalsegmentation::SemanticEventChainGeneratorInterfacePrx>(getProperty<std::string>("SECGeneratorName").getValue());
//    SECtoPEMConverterPrx = getProxy<hierarchicalsegmentation::SECtoPEMConverterInterfacePrx>("SECtoPEMConverter");

    oacDatabase = longtermMemory->getOacSegment()->getAll();
    ARMARX_VERBOSE << "Loaded " << oacDatabase.size() << " OACs from Mongo";


//    getMarkerBasedSECs();
    getMMMBasedSECs();

//    PemIce::StatePropertyList PEMWorlState = SECtoPEMConverterPrx->convertRelations(keyframes.at(190)->worldState);
//    for(PemIce::StatePropertyList::iterator it = PEMWorlState.begin(); it != PEMWorlState.end(); it++)
//    {
//        PemIce::StateProperty state = *it;
//        ARMARX_INFO << (state.sign?"":"NOT")<< state.name << " with: " << state.args ;
//    }
}


void SemanticEventChainInterpreter::onDisconnectComponent()
{
    priorKnowledge = NULL;
    objClassSegment = NULL;
}


void SemanticEventChainInterpreter::onExitComponent()
{

}

bool SemanticEventChainInterpreter::compareRelations(memoryx::SECRelationBasePtr relation1, memoryx::SECRelationBasePtr relation2) const
{
    if(!objClassSegment)
        throw LocalException("objClassSegment proxy is null");
    if(relation1->getName() != relation2->getName())
        return false;
    bool foundMatchObj1 = false;
    for(ObjectClassList::const_iterator it = relation1->objects1.begin(); it != relation1->objects1.end(); it++)
    {
        for(ObjectClassList::const_iterator itOther = relation2->objects1.begin(); itOther != relation2->objects1.end(); itOther++)
        {
            if(objClassSegment->compare(*it,*itOther) == eEqualClass || objClassSegment->compare(*itOther,*it) == eEqualClass)
                foundMatchObj1 = true;
        }
    }

    bool foundMatchObj2 = false;
    for(ObjectClassList::const_iterator it = relation1->objects2.begin(); it != relation1->objects2.end(); it++)
    {
        for(ObjectClassList::const_iterator itOther = relation2->objects2.begin(); itOther != relation2->objects2.end(); itOther++)
        {
            if(objClassSegment->compare(*it,*itOther) == eEqualClass || objClassSegment->compare(*itOther,*it) == eEqualClass)
                foundMatchObj2 = true;
        }
    }

    return foundMatchObj1 && foundMatchObj2;


}

Ice::Float SemanticEventChainInterpreter::compareObjectRelations(SECObjectRelationsBasePtr relationsSet, SECObjectRelationsBasePtr relationsSubset, SECRelationPairList &matchingRelations) const
{
    float matchingRelationCount = 0;

    for(SECRelationList::const_iterator itOther = relationsSubset->relations.begin(); itOther != relationsSubset->relations.end(); itOther++)
    {
        ARMARX_VERBOSE << "Trying to find match for " << (*itOther)->output() ;
        bool foundMatch = false;
//        SECRelationList::const_iterator matchIterator;
        bool foundConflict = false;
        SECRelationBasePtr otherRel = *itOther;
        for(SECRelationList::const_iterator it = relationsSet->relations.begin(); it != relationsSet->relations.end(); it++)
        {
//            ARMARX_VERBOSE << "Comparing " << (*itOther)->output() << " with " << (*it)->output();
            if(otherRel->isEqual(*it))
            {
                ARMARX_VERBOSE <<"equal Relation";
                foundMatch = true;
//                matchIterator = it;
                SECRelationPair pair;
                pair.relation1 = *it;
                pair.relation2 = *itOther;
                ARMARX_VERBOSE << "Adding relation pair of " << pair.relation1->output() << " and " << pair.relation2->output();
                matchingRelations.push_back(pair);
            }
//            else if(otherRel->hasEqualObjects(*it))
//            {
//                ARMARX_VERBOSE_S <<"found conflict";
//                foundConflict = true;
//            }

        }
        if(foundMatch && !foundConflict)
        {
            matchingRelationCount++;

        }
    }

    return matchingRelationCount/(float)relationsSubset->relations.size();


}

double SemanticEventChainInterpreter::checkSideConstraints(SECObjectRelationsBasePtr OACPreconditions, SECObjectRelationsBasePtr OACEffects, OacBasePtr oac, SECRelationPairList matchingPreconditions, SECRelationPairList matchingEffects, ObjectClassList &involvedObjects) const
{
    double constraintFactor = 1;
    SECRelationPairList sideConstraints = oac->getPredictionFunction()->getSECSideConstraints();
    for(unsigned int i = 0; i < sideConstraints.size(); i++)
    {
//        bool constraintFullfilled = false;
        SECRelationPair pair = sideConstraints.at(i);
//        SECRelationBasePtr relPre;
//        if(OACPreconditions->containsRelation(pair.relation1))
//            relPre = pair.relation1;
//        else
//        {
//            ARMARX_INFO_S << "Relation not found - skipping constraint";
//            continue;
//        }

        SECRelationBasePtr relPre = OACPreconditions->containsRelationBetweenObjects(pair.relation1->objects1, pair.relation1->objects2);
        SECRelationList worldStateRelationsPre;
        for(unsigned int j = 0; j < matchingPreconditions.size(); j++)
        {
            if(matchingPreconditions.at(j).relation2->isEqual(relPre))
            {
                worldStateRelationsPre.push_back(matchingPreconditions.at(j).relation1);
            }
//            else
//            {
//                matchingPreconditions.erase(matchingPreconditions.begin()+j);
//                j--;
//            }
        }

        SECRelationBasePtr relEffects = OACEffects->containsRelationBetweenObjects(pair.relation2->objects1, pair.relation2->objects2);
        SECRelationBasePtr worldStateRelationPre;
        SECRelationBasePtr worldStateRelationEffect;
        for(unsigned int j = 0; j < matchingEffects.size(); j++)
        {
            for(unsigned int k = 0; k < worldStateRelationsPre.size(); k++)
            {
//                if(relEffects->isEqual(matchingEffects.at(j).relation2)
                if(matchingEffects.at(j).relation2->isEqual(relEffects)
                        && matchingEffects.at(j).relation1->hasEqualObjects(worldStateRelationsPre.at(k)))
                {
                    worldStateRelationEffect = matchingEffects.at(j).relation1;
                    worldStateRelationPre = worldStateRelationsPre.at(k);
                    break;
                }
                //            else
                //            {
                //                matchingEffects.erase(matchingEffects.begin()+j);
                //                j--;
                //            }
            }
        }
        if(!worldStateRelationEffect)
        {
            constraintFactor -= 0.4;
            continue;
        }
        ARMARX_INFO <<"worldStateRelationPre: " << worldStateRelationPre->output();
        ARMARX_INFO <<"worldStateRelationEffect: " << worldStateRelationEffect->output();
        if(!worldStateRelationEffect->hasEqualObjects(worldStateRelationPre))
           constraintFactor -= 0.4;
        else
        {
            involvedObjects.insert(involvedObjects.end(), worldStateRelationPre->objects1.begin(),  worldStateRelationPre->objects1.end());
            involvedObjects.insert(involvedObjects.end(), worldStateRelationPre->objects2.begin(),  worldStateRelationPre->objects2.end());
        }

    }
    return constraintFactor;
}

void SemanticEventChainInterpreter::getMarkerBasedSECs()
{
    ViconCSVParser csvParser;

    std::string absolutePath;
    if(!getProperty<std::string>("ViconCSVFile").getValue().empty())
    {
        ArmarXDataPath::getAbsolutePath(getProperty<std::string>("ViconCSVFile").getValue(), absolutePath);
        csvParser.readFromCSVFile(absolutePath, false);
        SECKeyFrameMap keyframes = secGenPrx->calculateSEC(csvParser.getLines());

        SECMotionSegmentList segments = findOACSequence(keyframes);
        for(SECMotionSegmentList::iterator it = segments.begin(); it != segments.end(); it++)
        {
            ARMARX_IMPORTANT << "OAC: " << (*(it->oacs.begin()))->getName();
        }
    }
}

void SemanticEventChainInterpreter::getMMMBasedSECs()
{


    std::string absolutePath;
    if(!getProperty<std::string>("MMMMotionFile").getValue().empty())
    {

        ArmarXDataPath::getAbsolutePath(getProperty<std::string>("MMMMotionFile").getValue(), absolutePath);
        hierarchicalsegmentation::StringList data;
//        const std::string xmlString  = loadFileContent(absolutePath);
//        data.push_back(xmlString);
        data.push_back(absolutePath);
        SECKeyFrameMap keyframes = secGenPrx->calculateSEC(data);
        ARMARX_IMPORTANT << "Result keyframe: " << keyframes;
        SECMotionSegmentList segments = findOACSequence(keyframes);
//        for(auto seg : segments)
//        {
//            ARMARX_IMPORTANT << "OAC: " << (*(seg.oacs.begin()))->getName();
//        }
        for(auto &seg : segments)
        {
            SECKeyFramePtr keyframe = SECKeyFramePtr::dynamicCast(seg.prevKeyFrame);
            ARMARX_IMPORTANT << "Unobservable OACs that fit to: " << seg.prevKeyFrame;
            OacBaseList oacs = findUnobservableOACs(keyframe);
            seg.oacs.insert(seg.oacs.end(), oacs.begin(), oacs.end());
//            for(OacBasePtr oac : oacs)
//            {
//                ARMARX_IMPORTANT << "unobservable: " << (oac ? oac->getName() : "NULL");
//            }
        }

        for(SECMotionSegment &seg : segments)
        {
            std::stringstream str;
            str << "Segment from : "  << seg.prevKeyFrame->index << "-" << seg.nextKeyFrame->index << "\nWorldState" << seg.prevKeyFrame << "\nOACs: ";
            std::for_each(seg.oacs.begin(), seg.oacs.end(), [&](OacBasePtr oac){ str << oac->getName() << ", ";});
            str << "\n involvedObjects: " << ObjectClassMemorySegment::ObjectsToString(seg.involvedObjects);
            ARMARX_IMPORTANT << str.str();
        }
    }
}

std::string SemanticEventChainInterpreter::loadFileContent(const std::string &filepath) const
{
    // load file
    std::ifstream in(filepath.c_str());

    if (!in.is_open())
    {
        ARMARX_ERROR << "Could not open file:" << filepath;
        return "";
    }

    std::stringstream buffer;
    buffer << in.rdbuf();
    std::string content(buffer.str());
    in.close();
    return content;
}

Ice::Float SemanticEventChainInterpreter::matchOAC(OacBasePtr oac, SECObjectRelationsBasePtr worldStateBefore, SECObjectRelationsBasePtr worldStateAfter, ObjectClassList& involvedObjects) const
{
    ARMARX_INFO << "Matching " << oac->getName() << " with worldstates";
    SECObjectRelationsBasePtr OACPreconditions = oac->getPredictionFunction()->getSECPreconditions();
    SECObjectRelationsBasePtr OACEffects = oac->getPredictionFunction()->getSECEffects();
    SECRelationPairList matchingPreconditions;
    ARMARX_INFO << "Matching PRECONDITIONS";
    float scorePreconditions = compareObjectRelations(worldStateBefore, OACPreconditions, matchingPreconditions);
    SECRelationPairList matchingEffects;
    ARMARX_INFO << "Matching EFFECTS";
    float scoreEffects = compareObjectRelations(worldStateAfter, OACEffects, matchingEffects);

    double constraintFactor = checkSideConstraints(OACPreconditions, OACEffects, oac, matchingPreconditions, matchingEffects, involvedObjects);

    ARMARX_INFO << "Precondition score: " << scorePreconditions;
    ARMARX_INFO << "Effects score: " << scoreEffects;
    ARMARX_INFO << "constraintFactor: " << constraintFactor;
    return (scorePreconditions + scoreEffects) * 0.5 * constraintFactor;
}

OacBasePtr SemanticEventChainInterpreter::findOAC(SECObjectRelationsBasePtr worldStateBefore, SECObjectRelationsBasePtr worldStateAfter, memoryx::ObjectClassList &involvedObjects) const
{
    float bestScore = 0.0f;
    OacBasePtr bestMatch;
    memoryx::OacBaseList::const_iterator it = oacDatabase.begin();
    ObjectClassList involvedObjectsBestTemp;
    for(; it != oacDatabase.end(); it++)
    {
        OacBasePtr oac = *it;
        ObjectClassList involvedObjectsTemp;
        float score = matchOAC(oac, worldStateBefore, worldStateAfter, involvedObjectsTemp);
        if(bestScore < score)
        {
            bestScore = score;
            bestMatch = oac;
            involvedObjectsBestTemp = involvedObjectsTemp;
        }
    }
    if(bestScore < OACMatchingThreshold)
    {
        bestMatch = new Oac("NULL");
    }
    involvedObjects.insert(involvedObjects.end(), involvedObjectsBestTemp.begin(), involvedObjectsBestTemp.end());
    ARMARX_IMPORTANT << "Best matching OAC: " << bestMatch->getName();
    return bestMatch;
}


SECMotionSegmentList SemanticEventChainInterpreter::findOACSequence(const SECKeyFrameMap &secs, const Ice::Current &c)
{
    SECMotionSegmentList result;

    SECKeyFrameMap::const_iterator itPrev = secs.begin();
    SECKeyFrameMap::const_iterator it = secs.begin();
    it++;
    int i=1;
    for(; it != secs.end(); it++)
    {
        SECKeyFramePtr keyframe = SECKeyFramePtr::dynamicCast(it->second);
        SECKeyFramePtr prevKeyframe = SECKeyFramePtr::dynamicCast(itPrev->second);
        SECMotionSegment seg;
        seg.prevKeyFrame = prevKeyframe;
        seg.nextKeyFrame = keyframe;
        OacBasePtr oac = findOAC(prevKeyframe->worldState, keyframe->worldState, seg.involvedObjects);
        seg.oacs.push_back(oac);
        result.push_back(seg);
        itPrev = it;
//        if(i>1)
//            return result;
        i++;
    }
    return result;
}

OacBaseList SemanticEventChainInterpreter::findUnobservableOACs(const SECKeyFrameBasePtr &keyframe)
{
    OacBaseList result(oacDatabase);
    ObjectClassList temp;
//    float t = OACMatchingThreshold;
//    select<OacBaseList, OacBasePtr>(result, [&](OacBasePtr oac){return matchOAC(oac, keyframe->worldState, keyframe->worldState, temp) > OACMatchingThreshold;});
    result.erase(std::remove_if(result.begin(),
                 result.end(),
                 [&,this](const OacBasePtr& oac){
        bool bresult = matchOAC(oac, keyframe->worldState, keyframe->worldState, temp) < OACMatchingThreshold;
        return bresult;
    }), result.end());
    for(auto oac : result)
    {
        ARMARX_INFO << "oac: " << oac->getName();
    }
    return result;
}

PropertyDefinitionsPtr SemanticEventChainInterpreter::createPropertyDefinitions()
{
    return PropertyDefinitionsPtr(new SemanticEventChainInterpreterPropertyDefinitions(
                                           getConfigIdentifier()));
}



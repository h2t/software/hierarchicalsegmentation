/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    HierarchicalSegmentation::ArmarXObjects::MultiSegmenter
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#include "MultiSegmenter.h"
#include <SegLib/segmentation.h>
#include <SegLib/abstractconfiguration.h>
#include <SegLib/segmentationlib.h>
#include <PCASeg/pcaseg.h>
#include <PCASeg/pcasegconfiguration.h>
#include <VelocityCrossing/velocitycrossing.h>
#include <VelocityCrossing/velocitycrossingconfiguration.h>
#include <DummySeg/dummyseg.h>


#include <MMM/Motion/MotionReaderXML.h>
#include <MMMSimoxTools/MMMSimoxTools.h>

#include <ArmarXCore/core/system/ArmarXDataPath.h>

#include <VirtualRobot/Robot.h>

using namespace armarx;
using namespace Seg;
using namespace memoryx;
using namespace MMM;


void MultiSegmenter::onInitComponent()
{



}


void MultiSegmenter::onConnectComponent()
{

}


void MultiSegmenter::onDisconnectComponent()
{

}


void MultiSegmenter::onExitComponent()
{

}

PropertyDefinitionsPtr MultiSegmenter::createPropertyDefinitions()
{
    return PropertyDefinitionsPtr(new MultiSegmenterPropertyDefinitions(
                                      getConfigIdentifier()));
}

void MultiSegmenter::baseConfigure(AbstractConfiguration *config) const
{

}

hierarchicalsegmentation::SegmentationResultList MultiSegmenter::segment(const std::string &MMMFilePath, const std::string &mainObjectName, const Ice::Current &)
{
    hierarchicalsegmentation::SegmentationResultList result;

    MotionReaderXML reader;
    std::string absFilePath;
    armarx::ArmarXDataPath::getAbsolutePath(MMMFilePath, absFilePath);
    MotionList motions = reader.loadAllMotions(absFilePath);
    std::vector<MMM::AbstractMotionPtr> abstractMotions;
    std::map<std::string, VirtualRobot::RobotPtr>  robots;




    for(MotionPtr& m:motions)
    {

        if(m->getName() == mainObjectName)
        {
            robots[mainObjectName] = MMM::SimoxTools::buildModel(m->getModel());
            abstractMotions.push_back(m);
        }
    }

    VirtualRobot::RobotPtr robot = robots[mainObjectName];
    auto sensors = robot->getSensors();




    std::vector<Seg::SegmentationPtr> algorithms;

    SegmentationPtr pcaSeg(new PCASeg("PCASeg", false));
    auto pca_config = new PCASegConfiguration(AbstractConfiguration::MMMXML);
    pcaSeg->setConfig(pca_config);


    SegmentationPtr zvc(new VelocityCrossing("Velocity Crossing", false));
    auto zvc_config = new VelocityCrossingConfiguration(AbstractConfiguration::MMMXML);
    zvc->setConfig(zvc_config);

    SegmentationPtr dummySeg(new DummySeg("DummySeg", false));
    auto dummy_config = new DummySegConfiguration();
    dummy_config->setNumSegmentFrames(200);
    dummySeg->setConfig(dummy_config);


    algorithms.push_back(dummySeg);
    algorithms.push_back(zvc);
    algorithms.push_back(pcaSeg);



    // general config

    for(SegmentationPtr& algo : algorithms)
    {
        algo->getConfig()->setKeyFrame(AbstractConfiguration::FIRST);

        algo->getConfig()->setMotions(abstractMotions);
        algo->getConfig()->setRobots(robots);

    }

    // PCA Config
    pca_config->setSegmentOnJoints(false);
    pca_config->setReducedDim(getProperty<int>("pca.reduceDimensionsTo").getValue());
    pca_config->setAlpha(getProperty<float>("pca.alpha").getValue());
    for(auto& sensor : sensors)
    {
        pca_config->addSelectedJoints(sensor->getName(), 0);
    }



    // ZVC config
    zvc_config->setSegmentOnJoints(false);
    zvc_config->setVelocityEps(getProperty<float>("zvc.velocityepsilon").getValue());
    zvc_config->setNumJointsNull(getProperty<int>("zvc.numZeroJoints").getValue());
    for(auto& sensor : sensors)
    {
        zvc_config->addSelectedJoints(sensor->getName(), 0);
    }



    for(SegmentationPtr& algo : algorithms)
    {
        auto keyframes = algo->segment();
        ARMARX_INFO << VAROUT(keyframes);
        std::vector<double> doubleKeyFrames;
        keyframes = algo->framesToKeyFrames(keyframes);
        for(auto& keyframe: keyframes)
            doubleKeyFrames.push_back(keyframe*0.01);
        result[algo->getName()][mainObjectName] = doubleKeyFrames;
    }

    return result;
}


/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    HierarchicalSegmentation::ArmarXObjects::MMMSECGenerator
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#include "MMMSECGenerator.h"

#include "ModelBasedTaskSegmentation.h"
#include "SemanticSegmentSegmenter.h"

#include <MMM/Motion/MotionReaderXML.h>

#include <MemoryX/libraries/memorytypes/segment/ObjectClassMemorySegment.h>
#include <MemoryX/core/MemoryXCoreObjectFactories.h>
#include <MemoryX/libraries/memorytypes/MemoryXTypesObjectFactories.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>

#include <dmp/io/MMMConverter.h>

#include <boost/archive/xml_oarchive.hpp>
#include <boost/archive/text_oarchive.hpp>


using namespace armarx;
using namespace memoryx;
using namespace MMM;
using namespace DMP;


void MMMSECGenerator::Plot(const std::vector<double>& keyframes, const DMP::SampledTrajectoryV2 & traj)
{
    std::vector<DVec> plots;
    std::vector<std::string> titles;
    //    frames.resize(450, 0);
    int deriv = 0;


    DVec frames = traj.getTimestamps();
    plots.push_back(frames);
    titles.push_back("frames");
    titles.push_back("X");
    titles.push_back("Y");
    titles.push_back("Z");
    for(size_t d = 0; d < traj.dim(); d++)
    {
        plots.push_back(traj.getDimensionData(d,deriv));
//        std::stringstream str;
//        str << "axis " << d;
//        titles.push_back(str.str());
    }


    std::stringstream add;
    add << "set xrange ["<< *frames.begin() << ":"<< *frames.rbegin() << "]\n";
//    add << "set yrange [" << traj.getMin(0,0, traj.begin()->getTimestamp(), traj.rbegin()->getTimestamp())  << ":"<< traj.getMax(0,0, traj.begin()->getTimestamp(), traj.rbegin()->getTimestamp()) << "]\n";
    add << "set key left top\n";
    add << " set xlabel 'Time in seconds'\n" ;
    add << " set ylabel 'Position in mm'\n";
    add << "set style line 6 lt 0 lw 3 lc rgb \"black\" \n";
    std::vector<double>::const_iterator it = keyframes.begin();
    int i=1;
    for(; it != keyframes.end(); it++)
    {
        add << "set arrow from " << *it << ",graph(0,0) to " << *it << ",graph(1,1) nohead ls 6;\n";
        i++;
    }
//    add << "set arrow from "<< *frames.begin() <<"," << distanceThreshold << " to "<< *frames.rbegin() << "," <<distanceThreshold << " nohead lt 5 lc rgb 'black';\n";

    // svg
//    if(imageType == eSVG)
//    {
//        add << "set terminal svg size 1800,600 fname 'Verdana' fsize 10\n";
//        add << "set output 'plots/comparison.svg'\n";
//    }
//    else
//    {
//        //    // eps
//        add << "set terminal postscript eps enhanced color font 'Verdana,20' linewidth 3" << std::endl
//            <<" set output 'plots/segmentation.eps'";
//    }
    std::stringstream str;
    str << "plots/aas_" << traj.getMin(0,0, traj.begin()->getTimestamp(), traj.rbegin()->getTimestamp())  << "_%d.gp";
    plotTogether(plots,titles,str.str().c_str(), true, add.str());
}

void MMMSECGenerator::onInitComponent()
{
    usingProxy("PriorKnowledge");
    fps = getProperty<float>("FPS").getValue();
}


void MMMSECGenerator::onConnectComponent()
{
    priorKnowledge = getProxy<PriorKnowledgeInterfacePrx>("PriorKnowledge");
    objectClassSegment = priorKnowledge->getObjectClassesSegment();
    return;
    ObjectClassList allObjects = objectClassSegment->getClassWithSubclasses("all");
    ObjectClassList allHands = objectClassSegment->getClassWithSubclasses("hand");
    ObjectClassList allContainer = objectClassSegment->getClassWithSubclasses("container");
    ARMARX_VERBOSE << "Hands: " << memoryx::ObjectClassMemorySegment::ObjectsToString(allHands);
    ARMARX_VERBOSE << "allContainer: " << memoryx::ObjectClassMemorySegment::ObjectsToString(allContainer);
    allObjects.insert(allObjects.end(), allHands.begin(), allHands.end());
    ARMARX_VERBOSE << "Objects: " << memoryx::ObjectClassMemorySegment::ObjectsToString(allObjects);

    MotionReaderXML reader;
//    MotionList motions = reader.loadAllMotions("/home/SMBAD/waechter/projects/MMMTools/data/Motions/Trial09_whisk_lying_MMM.xml");
//    MotionList motions = reader.loadAllMotions("/home/SMBAD/waechter/projects/MMMTools/data/Motions/Trial12_whisk_lying_MMM.xml");
    auto ws = getWorldStates("/home/waechter/projects/HierarchicalSegmentation/data/ObjectsWithMarkers/Motions/Shaking_Trial10.xml", "rightHand", GetDefaultConfig());
    return;
    MotionList motions = reader.loadAllMotions("/home/waechter/projects/HierarchicalSegmentation/data/ObjectsWithMarkers/Motions/Shaking_Trial10.xml");
//    MotionList motions = reader.loadAllMotions("/home/waechter/projects/MMMTools/data/Motions/Wiping_Trial04_MMM.xml");
//        MotionList motions = reader.loadAllMotions("/home/waechter/projects/MMMTools/data/Motions/Wiping_Trial01_MMM.xml");

////    MotionList motions = reader.loadAllMotions("/home/waechter/projects/MMMTools/build/bin/Trial09_whisk_lying_MMM.xml");

//    MotionList motions = reader.loadAllMotions("/home/waechter/projects/MMMTools/build/bin/Trial06_MMM.xml");

    hierarchicalsegmentation::DoubleList params = GetDefaultConfig();
    ModelBasedTaskSegmentation seg(motions,
                                   params[hierarchicalsegmentation::eDistanceThreshold],
                                   params[hierarchicalsegmentation::eSemanticMergeThreshold],
                                   params[hierarchicalsegmentation::eHystereseFactor]);
    seg.setObjects(allObjects);
    seg.calc();
//    seg.plot();


    SemanticSegmentSegmenter segm(params[hierarchicalsegmentation::eWindowSize],
            params[hierarchicalsegmentation::eMinSegmentSize],
            params[hierarchicalsegmentation::eDivideThreshold],
            params[hierarchicalsegmentation::eDerivation],
            params[hierarchicalsegmentation::eWeightRoot],
            params[hierarchicalsegmentation::eGaussFilterWidth]);
    ModelBasedTaskSegmentation::MotionDataMap motionData;
    segm.setWorldStates(seg.getKeyFrames());
//    motionData["leftHand"] = seg.getMotionData().at("leftHand");
    motionData["rightHand"] = seg.getMotionData().at("rightHand");
    segm.setMotions(motionData);
    segm.setSemanticKeyframes(seg.getKeyFramesGroupedByObject());
    segm.calcKeyframes();
    auto keyframes = segm.getAllKeyFrames();
    ARMARX_IMPORTANT << VAROUT(keyframes);
    ARMARX_IMPORTANT << VAROUT(segm.convertToFlatSegmentation());
//    std::ofstream file("Shaking_Trial10_MMM_segmentation.xml");
//    boost::archive::xml_oarchive ar(file);
//    ar <<  boost::serialization::make_nvp("SegData", segm.getCompleteSegmentationData());
}


void MMMSECGenerator::onDisconnectComponent()
{

}


void MMMSECGenerator::onExitComponent()
{

}

PropertyDefinitionsPtr MMMSECGenerator::createPropertyDefinitions()
{
    return PropertyDefinitionsPtr(new MMMSECGeneratorPropertyDefinitions(
                                      getConfigIdentifier()));
}

hierarchicalsegmentation::DoubleList MMMSECGenerator::GetDefaultConfig()
{
    hierarchicalsegmentation::DoubleList params(hierarchicalsegmentation::eSemanticConfigParamCount, 0.0);
    params[hierarchicalsegmentation::eDistanceThreshold] = 15;
    params[hierarchicalsegmentation::eSemanticMergeThreshold] = 47;
    params[hierarchicalsegmentation::eHystereseFactor] = 1.72;
    params[hierarchicalsegmentation::eWindowSize] = 0.54;
    params[hierarchicalsegmentation::eMinSegmentSize] = 1.09;
    params[hierarchicalsegmentation::eDivideThreshold] = 819;
    params[hierarchicalsegmentation::eDerivation] = 1.22;
    params[hierarchicalsegmentation::eWeightRoot] = 3.7;
    params[hierarchicalsegmentation::eGaussFilterWidth] = 0.078;

    return params;
}



SECKeyFrameMap armarx::MMMSECGenerator::calculateSEC(const hierarchicalsegmentation::StringList &MMMMotionXMLString, const Ice::Current &c = Ice::Current())
{

    ObjectClassList allObjects = objectClassSegment->getClassWithSubclasses("all");
    ObjectClassList allHands = objectClassSegment->getClassWithSubclasses("hand");
    allObjects.insert(allObjects.end(), allHands.begin(), allHands.end());
    MotionReaderXML reader;
    MotionList motions = reader.loadAllMotions(MMMMotionXMLString.at(0));
    ModelBasedTaskSegmentation seg(motions);
    seg.setObjects(allObjects);
    seg.calc();
    return seg.getKeyFrames();
}

hierarchicalsegmentation::ObjectKeyframes MMMSECGenerator::calcKeyframes(const std::string &MMMFilePath, const Ice::Current &c)
{

    hierarchicalsegmentation::DoubleList params = GetDefaultConfig();

    return calcKeyframesWithConfig(MMMFilePath, params, c);
}

hierarchicalsegmentation::Keyframes MMMSECGenerator::getXPositions(const std::string &MMMFile, const std::string &objectName, const Ice::Current &)
{
    MotionReaderXML reader;
    std::string absFilePath;
    armarx::ArmarXDataPath::getAbsolutePath(MMMFile, absFilePath);
    MotionPtr motion = reader.loadMotion(absFilePath, objectName);
    hierarchicalsegmentation::Keyframes result;
    for (int i = 0; i < motion->getNumFrames(); ++i) {
        result.push_back(motion->getMotionFrame(i)->getRootPos()[0]);
    }
    return result;

}

hierarchicalsegmentation::ObjectKeyframes MMMSECGenerator::calcKeyframesWithConfig(const std::string &MMMFilePath, const hierarchicalsegmentation::DoubleList &params, const Ice::Current &)
{
    ARMARX_CHECK_EXPRESSION(params.size() == hierarchicalsegmentation::eSemanticConfigParamCount);
    ObjectClassList allObjects = objectClassSegment->getClassWithSubclasses("all");

    ARMARX_INFO << VAROUT(MMMFilePath);
    ARMARX_INFO << VAROUT(params);

    MotionList motions = getMotionFile(MMMFilePath);

    ModelBasedTaskSegmentation seg(motions,
                                   params[hierarchicalsegmentation::eDistanceThreshold],
                                   params[hierarchicalsegmentation::eSemanticMergeThreshold],
                                   params[hierarchicalsegmentation::eHystereseFactor]);
    seg.setObjects(allObjects);
    seg.calc();
    seg.plot();


    SemanticSegmentSegmenter segm(params[hierarchicalsegmentation::eWindowSize],
            params[hierarchicalsegmentation::eMinSegmentSize],
            params[hierarchicalsegmentation::eDivideThreshold],
            params[hierarchicalsegmentation::eDerivation],
            params[hierarchicalsegmentation::eWeightRoot],
            params[hierarchicalsegmentation::eGaussFilterWidth]);
    ModelBasedTaskSegmentation::MotionDataMap motionData;
//    motionData["leftHand"] = seg.getMotionData().at("leftHand");
    motionData["rightHand"] = seg.getMotionData().at("rightHand");
    segm.setMotions(motionData);
    segm.setSemanticKeyframes(seg.getKeyFramesGroupedByObject());
    segm.calcKeyframes();
    auto keyframes = segm.getAllKeyFrames();
    return keyframes;
}

MotionList MMMSECGenerator::getMotionFile(std::string filePath)
{
    ScopedRecursiveLock lock(motionsMutex);
    armarx::ArmarXDataPath::getAbsolutePath(filePath, filePath);
    auto it = motionFiles.find(filePath) ;
    if(it == motionFiles.end())
    {
        MotionReaderXML reader;
        MotionList motions = reader.loadAllMotions(filePath);

        // convert all motion names to lower case (due to wrong capitalization)
        for (MotionPtr& curMotion : motions)
        {
            std::string name = curMotion->getName();;
            std::transform(name.begin(), name.end(), name.begin(), ::tolower);
            curMotion->setName(name);
        }

        motionFiles[filePath] = motions;
        return motionFiles[filePath];
    }
    else
        return it->second;
}

hierarchicalsegmentation::KeyframesWithWorldStateVec MMMSECGenerator::convertToSimpleWorldStates(const memoryx::SECKeyFrameMap &keyframes)
{
    using namespace hierarchicalsegmentation;
    KeyframesWithWorldStateVec result;
    for(const auto &keyframepair : keyframes)
    {
        hierarchicalsegmentation::RelationList relations;
        SECRelationList secrelations = keyframepair.second->worldState->relations;
        for(memoryx::SECRelationBasePtr relation : secrelations)
        {
            hierarchicalsegmentation::Relation newRel;
            newRel.type = relation->getName();
            newRel.object1 = relation->objects1.front()->getName();
            newRel.object2 = relation->objects2.front()->getName();
            relations.push_back(newRel);
        }
        SimpleWorldState ws{relations, keyframepair.first/fps};

        result.push_back(ws);
    }

    return result;
}

hierarchicalsegmentation::KeyframesWithWorldStateVec MMMSECGenerator::calcWorldStatesAtFrames(const std::string &MMMFilePath, const hierarchicalsegmentation::DoubleList &frames, const hierarchicalsegmentation::DoubleList& segParams, const Ice::Current &)
{
    auto params = segParams;
    if (params.size() == 0)
        params = GetDefaultConfig();
    ARMARX_CHECK_EXPRESSION(params.size() == hierarchicalsegmentation::eSemanticConfigParamCount);
    ObjectClassList allObjects = objectClassSegment->getClassWithSubclasses("all");

    ARMARX_INFO << VAROUT(MMMFilePath);
    ARMARX_INFO << VAROUT(params);

    MotionList motions = getMotionFile(MMMFilePath);
    ModelBasedTaskSegmentation seg(motions,
                                   params[hierarchicalsegmentation::eDistanceThreshold],
                                   params[hierarchicalsegmentation::eSemanticMergeThreshold],
                                   params[hierarchicalsegmentation::eHystereseFactor]);
    seg.setObjects(allObjects);
    auto worldstates = seg.calcWorldStatesAtTimestamps(frames);
    hierarchicalsegmentation::KeyframesWithWorldStateVec result = convertToSimpleWorldStates(worldstates);
    return result;
}

hierarchicalsegmentation::KeyframesWithWorldStateVec MMMSECGenerator::getWorldStates(const std::string &MMMFilePath, const std::string &objectName, const hierarchicalsegmentation::DoubleList &params, const Ice::Current &)
{
    ARMARX_CHECK_EXPRESSION(params.size() == hierarchicalsegmentation::eSemanticConfigParamCount);
    ObjectClassList allObjects = objectClassSegment->getClassWithSubclasses("all");

    ARMARX_INFO << VAROUT(MMMFilePath);
    ARMARX_INFO << VAROUT(params);

    MotionList motions = getMotionFile(MMMFilePath);

    ModelBasedTaskSegmentation seg(motions,
                                   params[hierarchicalsegmentation::eDistanceThreshold],
                                   params[hierarchicalsegmentation::eSemanticMergeThreshold],
                                   params[hierarchicalsegmentation::eHystereseFactor]);
    seg.setObjects(allObjects);
    seg.calc();
    seg.plot();


    SemanticSegmentSegmenter segm(params[hierarchicalsegmentation::eWindowSize],
            params[hierarchicalsegmentation::eMinSegmentSize],
            params[hierarchicalsegmentation::eDivideThreshold],
            params[hierarchicalsegmentation::eDerivation],
            params[hierarchicalsegmentation::eWeightRoot],
            params[hierarchicalsegmentation::eGaussFilterWidth]);
    ModelBasedTaskSegmentation::MotionDataMap motionData;
//    motionData["leftHand"] = seg.getMotionData().at("leftHand");
    motionData[objectName] = seg.getMotionData().at(objectName);
    segm.setMotions(motionData);
    segm.setSemanticKeyframes(seg.getKeyFramesGroupedByObject());
    segm.calcKeyframes();
    segm.setWorldStates(seg.getKeyFrames());

    memoryx::SECKeyFrameMap keyframes = segm.convertToFlatSegmentation();
    hierarchicalsegmentation::KeyframesWithWorldStateVec result = convertToSimpleWorldStates(keyframes);
    return result;
}


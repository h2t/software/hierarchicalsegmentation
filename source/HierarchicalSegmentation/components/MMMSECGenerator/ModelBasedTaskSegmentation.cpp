/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#include "ModelBasedTaskSegmentation.h"

#include <VirtualRobot/CollisionDetection/CDManager.h>
#include <VirtualRobot/Robot.h>

#include <MMMSimoxTools/MMMSimoxTools.h>

#include <boost/foreach.hpp>
#include <boost/type_traits.hpp>

#include <MemoryX/libraries/memorytypes/entity/SEC/SECObjectRelations.h>
#include <MemoryX/libraries/memorytypes/entity/SEC/SECRelation.h>
#include <MemoryX/libraries/memorytypes/entity/SEC/SECKeyFrame.h>

#include <HierarchicalSegmentation/libraries/SemanticEventChains/AutomaticActionSegmentation.h>
#include <HierarchicalSegmentation/libraries/SemanticEventChains/GnuPlotWrapper.h>

#include <dmp/general/vec.h>
#include <dmp/general/helpers.h>


using namespace MMM;
using namespace VirtualRobot;
using namespace memoryx;

namespace armarx {

    ModelBasedTaskSegmentation::ModelBasedTaskSegmentation(const MotionList &motions, float collisionThreshold, float mergeThreshold, float hystereseFactor) :

        cdMan(new CDManager()),
        collisionThreshold(collisionThreshold),
        mergeThreshold(mergeThreshold),
        hystereseFactor(hystereseFactor)
    {
        BOOST_FOREACH(MotionPtr motion, motions)
        {
            MotionData data;
            data.motion = motion;
            data.model = SimoxTools::buildModel(motion->getModel());

            _motions[data.motion->getName()] = data;
            ARMARX_DEBUG << "Found motion with name " << data.motion->getName() << " and " << data.motion->getNumFrames() << " frames";
        }
    }

    void ModelBasedTaskSegmentation::setObjects(const ObjectClassList &objects)
    {
        _objects.clear();
        ObjectClassList::const_iterator it = objects.begin();
        for(; it != objects.end(); it++)
        {
            _objects[(*it)->getName()] = *it;
        }
    }

    void ModelBasedTaskSegmentation::updateKeyframeGroupedByObject()
    {
        for(NameIntSetMap::iterator itSub = _objectGroupedKeyFramekeyframes.begin(); itSub != _objectGroupedKeyFramekeyframes.end(); itSub++)
        {
            std::set<int>& kfSet = itSub->second ;

            std::set<int> delSet;
            for(std::set<int>::iterator it = kfSet.begin(); it != kfSet.end(); it++)
            {
                if(_keyframes.find(*it) == _keyframes.end())
                {
                    delSet.insert(*it);
                }

            }
            for(std::set<int>::iterator it = delSet.begin(); it != delSet.end(); it++)
            {
                kfSet.erase(*it);
            }
        }
    }

    void ModelBasedTaskSegmentation::calc()
    {
        _findKeyFrames();

        AutomaticActionSegmentation::MergeKeyFrames(_keyframes, mergeThreshold);
        updateKeyframeGroupedByObject();
        _keyframes = calcCumulativeWorldStates();
    }

    void ModelBasedTaskSegmentation::plot()
    {
        using namespace DMP;


        std::vector<std::vector<double> >  plots;
        std::vector<std::string> titles;

        std::vector<double> frames;
        MotionDataMap::iterator i1 = _motions.begin();
        for (size_t i=0; i1 != _motions.end() && i < i1->second.motion->getNumFrames(); ++i) {
            frames.push_back(i1->second.motion->getMotionFrame(i)->timestep);
        }

        if(frames.size() == 0)
        {
            ARMARX_ERROR << "no frames found - skipping plotting";
            return;
        }

        plots.push_back(frames);
        titles.push_back("frames");


        for (std::set<std::pair<std::string, std::string> >::iterator it = _objectsWithContact.begin();
             it != _objectsWithContact.end();
             ++it) {
            titles.push_back(it->first + " to " + it->second);
            plots.push_back(_objectDistances[*it]);
        }


        std::stringstream add;
        add << "set xrange ["<< *frames.begin() << ":"<< *frames.rbegin() << "]\n";
    //    add << "set yrange [" << origTraj->getMin(0,0, origTraj->begin()->getTimestamp(), origTraj->rbegin()->getTimestamp())  << ":"<< origTraj->getMax(0,0, origTraj->begin()->getTimestamp(), origTraj->rbegin()->getTimestamp()) << "]\n";
        add << "set key right bottom\n";
        add << " set xlabel 'Time in seconds'\n" ;
        add << " set ylabel 'Distance in mm'\n";
        add << "set style line 6 lt 0 lw 3 lc rgb \"black\" \n";

        int i=1;
        for(memoryx::SECKeyFrameMap::iterator it = _keyframes.begin();
            it != _keyframes.end(); it++)
        {
            add << "set arrow from " << it->first*0.01 << ",graph(0,0) to " << it->first*0.01 << ",graph(1,1) nohead ls 6;\n";
            i++;
        }
//        add << "set arrow from "<< *frames.begin() <<"," << collisionThreshold << " to "<< *frames.rbegin() << "," <<collisionThreshold << " nohead lt 5 lc rgb 'black';\n";

        // svg
    //    if(imageType == eSVG)
    //    {
    //        add << "set terminal svg size 1800,600 fname 'Verdana' fsize 10\n";
    //        add << "set output 'plots/comparison.svg'\n";
    //    }
    //    else
    //    {
    //        //    // eps
//            add << "set terminal postscript eps enhanced color font 'Verdana,20' linewidth 3" << std::endl
//                <<" set output 'plots/segmentation.eps'";
    //    }
        std::stringstream str;
        str << "plots/aas_%d.gp";

        GnuPlotWrapper::plotTogether(plots,titles,str.str().c_str(), true, add.str());
    }

    SECKeyFrameMap ModelBasedTaskSegmentation::getKeyFrames() const
    {
        return _keyframes;
    }

    std::set<int> ModelBasedTaskSegmentation::getKeyFramesForObject(const std::string &objName) const
    {
        NameIntSetMap::const_iterator it =_objectGroupedKeyFramekeyframes.find(objName);
        if(it != _objectGroupedKeyFramekeyframes.end())
            return it->second;
        else
            throw LocalException() << "no keyframe for object with name " << objName << " found";
    }

    NameIntSetMap ModelBasedTaskSegmentation::getKeyFramesGroupedByObject() const
    {
        return _objectGroupedKeyFramekeyframes;
    }

    SECKeyFrameMap ModelBasedTaskSegmentation::calcWorldStatesAtTimestamps(const Ice::DoubleSeq timestamps) const
    {

        memoryx::SECKeyFrameMap result;
        if(timestamps.size() == 0)
            return result;
        MotionDataMap::const_iterator obj1 = _motions.begin();
        for (; obj1 != _motions.end(); ++obj1) {
            MotionDataMap::const_iterator obj2 = obj1;
            obj2++;
            for( ; obj2 != _motions.end(); ++obj2) {
                if(obj1->first == obj2->first)
                {
                    ARMARX_VERBOSE << "Same obj";
                    continue;
                }
                ARMARX_INFO << "Checking objects: " << obj1->first << " and " << obj2->first;
                MotionPtr obj1Motion = obj1->second.motion;
                MotionPtr obj2Motion = obj2->second.motion;
                RobotPtr obj1Model = obj1->second.model;
                RobotPtr obj2Model = obj2->second.model;
                ObjectClassList objects1;
                objects1.push_back(_getObject(obj1->first));
                ObjectClassList objects2;
                objects2.push_back(_getObject(obj2->first));
                auto cdm = _prepareCollisionDetection(obj1, obj2);
                size_t tIndex = 0;
                for(size_t frameIndex = 0; frameIndex < obj1Motion->getNumFrames() && tIndex < timestamps.size(); frameIndex++)
                {

                    if(obj1Motion->getMotionFrame(frameIndex)->timestep - timestamps.at(tIndex) < -0.001)
                    {
//                        ARMARX_INFO << "timestamps at index " << frameIndex+1 << "/" << obj1Motion->getNumFrames() << " and " << tIndex+1 << "/" << timestamps.size() << " dont match: " << obj1Motion->getMotionFrame(frameIndex)->timestep << " vs. " << timestamps.at(tIndex) << " diff: " << obj1Motion->getMotionFrame(frameIndex)->timestep - timestamps.at(tIndex);
                        continue;
                    }
                    tIndex++;
//                    double frame = timestamps[frameIndex];
                    obj1Model->setGlobalPose(obj1Motion->getMotionFrame(frameIndex)->getRootPose());
                    obj2Model->setGlobalPose(obj2Motion->getMotionFrame(frameIndex)->getRootPose());
                    std::vector<SECRelationBasePtr> newRelations;
                    float distance = cdm.getDistance();
                    if(distance < collisionThreshold)
                    {
                        ARMARX_IMPORTANT << "#" << frameIndex << " Contact detected: distance " << distance;
                        newRelations.push_back(new Relations::TouchingRelation(objects1, objects2));
                    }
                    else
                    {
                        ARMARX_IMPORTANT << "#" << frameIndex << " No Contact: distance " << distance;
                        newRelations.push_back(new Relations::NoConnectionRelation(objects1, objects2));

                    }

                    SECKeyFrameMap::iterator it = result.find(frameIndex);
                    if(it == result.end())
                        result[frameIndex] = new SECKeyFrame();
                    result[frameIndex]->index = frameIndex;
                    result[frameIndex]->worldState->addRelations(newRelations);

                }
            }
        }
        return result;
    }

    void ModelBasedTaskSegmentation::_findKeyFrames()
    {
        _keyframes.clear();
        SECKeyFramePtr kf = new SECKeyFrame();
        kf->index = 0;
        _keyframes[0] = kf;
        ARMARX_DEBUG << "Searching keyframes";
        MotionDataMap::iterator i1 = _motions.begin();
        for (; i1 != _motions.end(); ++i1) {
            MotionDataMap::iterator i2 = i1;
            i2++;
            for( ; i2 != _motions.end(); ++i2) {
                //            SECKeyFrameMap keyframes;
                _findKeyFrames(i1, i2,_keyframes);
                //            __keyframes.insert(keyframes.begin(), keyframes.end());
            }
        }
    }

    CDManager ModelBasedTaskSegmentation::_prepareCollisionDetection(MotionDataMap::const_iterator obj1, MotionDataMap::const_iterator obj2) const
    {
        CDManager cdm;
        RobotPtr obj1Model = obj1->second.model;
        RobotPtr obj2Model = obj2->second.model;



        std::vector<RobotNodePtr> colModels1 = obj1Model->getRobotNodes();
        SceneObjectSetPtr sos1(new SceneObjectSet());
        BOOST_FOREACH(RobotNodePtr colModel, colModels1)
        {
            if(colModel->getCollisionModel())
                sos1->addSceneObject(colModel);
        }
        std::vector<RobotNodePtr> colModels2 = obj2Model->getRobotNodes();
        SceneObjectSetPtr sos2(new SceneObjectSet());
        BOOST_FOREACH(RobotNodePtr colModel, colModels2)
        {
            if(colModel->getCollisionModel())
                sos2->addSceneObject(colModel);
        }

        cdm.addCollisionModel(sos1);
        cdm.addCollisionModel(sos2);
        return cdm;
    }

    void ModelBasedTaskSegmentation::_findKeyFrames(MotionDataMap::iterator obj1, MotionDataMap::iterator obj2, memoryx::SECKeyFrameMap &outputKeyFrames)
    {
        if(obj1->first == obj2->first)
        {
            ARMARX_VERBOSE << "Same obj";
            return;
        }
        ARMARX_INFO << "Checking objects: " << obj1->first << " and " << obj2->first;

        /////////////////////////////////
        /// Setup
        /////////////////////////////////
        MotionPtr obj1Motion = obj1->second.motion;
        MotionPtr obj2Motion = obj2->second.motion;
        RobotPtr obj1Model = obj1->second.model;
        RobotPtr obj2Model = obj2->second.model;
        CDManager cdm(_prepareCollisionDetection(obj1, obj2));
        ObjectClassList objects1;
        objects1.push_back(_getObject(obj1->first));
        ObjectClassList objects2;
        objects2.push_back(_getObject(obj2->first));

        /////////////////////////////////
        /// Calculation
        /////////////////////////////////

        bool contact = false;
        const size_t frameCount = obj1Motion->getNumFrames();
        for(size_t frame = 0; frame < frameCount; frame++)
        {
            obj1Model->setGlobalPose(obj1Motion->getMotionFrame(frame)->getRootPose());
            obj2Model->setGlobalPose(obj2Motion->getMotionFrame(frame)->getRootPose());
            std::vector<SECRelationBasePtr> newRelations;
            float distance = cdm.getDistance();
            bool newKeyFrame = false;
            if(!contact && distance < collisionThreshold)
            {
                ARMARX_IMPORTANT << "#" << frame << " Contact detected: " << distance;
                newKeyFrame = true;
                contact = true;
                newRelations.push_back(new Relations::TouchingRelation(objects1, objects2));
            }
            else if(contact && distance > collisionThreshold * hystereseFactor)
            {
                ARMARX_IMPORTANT << "#" << frame << " Contact lost: " << distance;
                newKeyFrame = true;
                contact = false;
                newRelations.push_back(new Relations::NoConnectionRelation(objects1, objects2));

            }
            if(newKeyFrame)
            {
                SECKeyFrameMap::iterator it = outputKeyFrames.find(frame);
                if(it == outputKeyFrames.end())
                    outputKeyFrames[frame] = new SECKeyFrame();
                outputKeyFrames[frame]->index = frame;
                outputKeyFrames[frame]->worldState->addRelations(newRelations);
                _objectGroupedKeyFramekeyframes[obj1->first].insert(frame);
                _objectGroupedKeyFramekeyframes[obj2->first].insert(frame);
                _objectsWithContact.insert(std::make_pair(obj1->first, obj2->first));
            }
             _objectDistances[std::make_pair(obj1->first, obj2->first)].push_back(distance);
        }
    }


    SECKeyFrameMap ModelBasedTaskSegmentation::calcCumulativeWorldStates()
    {
        SECKeyFrameMap result;
        SECKeyFrameBasePtr currentKeyFrame = new SECKeyFrame();
        MotionDataMap::iterator i1 = _motions.begin();
        for (; i1 != _motions.end(); ++i1) {
            MotionDataMap::iterator i2 = i1;
            for(i2++ ; i2 != _motions.end(); ++i2) {
                ObjectClassList objects1;
                objects1.push_back(_getObject(i1->first));
                ObjectClassList objects2;
                objects2.push_back(_getObject(i2->first));
    //            currentKeyFrame->worldState->relations.push_back(new Relations::TouchingRelation(objects1, objects2));
                currentKeyFrame->worldState->addRelation(new Relations::NoConnectionRelation(objects1, objects2));
    //            ARMARX_DEBUG_S << "Adding relation: " << (*currentKeyFrame->worldState->relations.rbegin())->output();
                //            currentKeyFrame.worldState->relations.push_back(Relation("Touching", i1->first, i2->first, true));
                //            currentKeyFrame.worldState->relations.push_back(Relation("Touching", i1->first, i2->first, true));
            }
        }

        auto it =  _keyframes.begin();
        for(; it != _keyframes.end(); it++)
        {
            const SECKeyFrameBasePtr& selectedKeyFrame = it->second;
            currentKeyFrame->index = selectedKeyFrame->index;
            ARMARX_IMPORTANT << selectedKeyFrame->index << " vs. " << it->first;
            currentKeyFrame->worldState->addRelations(selectedKeyFrame->worldState->relations);
    //        for(unsigned int i=0; i < selectedKeyFrame->worldState->relations.size(); i++)
    //        {
    //            currentKeyFrame->worldState->addRelation(selectedKeyFrame->worldState->relations.at(i));
    //            //            int relIndex = currentKeyFrame.worldState->containsRelation(selectedKeyFrame.worldState->relations.at(i).object1, selectedKeyFrame.worldState->relations.at(i).object2);
    //            //            if(relIndex != -1)
    //            //                currentKeyFrame.worldState->relations.at(relIndex) = selectedKeyFrame.worldState->relations.at(i);
    //            //            else
    //            //                currentKeyFrame.worldState->relations.push_back(selectedKeyFrame.worldState->relations.at(i));
    //        }
            result[it->first] = SECKeyFrameBasePtr::dynamicCast(currentKeyFrame->ice_clone());
        }
        return result;
    }

    ObjectClassBasePtr ModelBasedTaskSegmentation::_getObject(const std::string &objectName) const
    {
        ObjectClassMap::const_iterator it = _objects.find(objectName);
        if(it != _objects.end())
            return it->second;
        else
            throw LocalException("Could not find object in map with name: ") << objectName;
        return NULL;
    }

}

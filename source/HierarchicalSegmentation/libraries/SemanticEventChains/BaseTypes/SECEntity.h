/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#ifndef ARMARX_SECENTITY_H
#define ARMARX_SECENTITY_H

#include <MemoryX/core/entity/Entity.h>

namespace armarx {

    class SECEntity :
            virtual public hierarchicalsegmentation::SECEntityBase,
            virtual public memoryx::Entity

    {
    public:
        SECEntity();

        // SECEntityBase interface
    public:
        hierarchicalsegmentation::SECEntityBasePtr getParentClass(const Ice::Current &);
        bool isEqual(const hierarchicalsegmentation::SECEntityBasePtr &, const Ice::Current &);

        // EntityBase interface
    public:
        std::string getId(const Ice::Current &) const;
        void setId(const std::string &, const Ice::Current &);
        std::string getName(const Ice::Current &) const;
        void setName(const std::string &, const Ice::Current &);
        memoryx::EntityAttributeBasePtr getAttribute(const std::string &, const Ice::Current &) const;
        bool hasAttribute(const std::string &, const Ice::Current &) const;
        void putAttribute(const memoryx::EntityAttributeBasePtr &, const Ice::Current &);
        void removeAttribute(const std::string &, const Ice::Current &);
        memoryx::NameList getAttributeNames(const Ice::Current &) const;

        // Serializable interface
    public:
        void serialize(const ObjectSerializerBasePtr &, const Ice::Current &) const;
        void deserialize(const ObjectSerializerBasePtr &, const Ice::Current &);
    };

} // namespace armarx

#endif // ARMARX_SECENTITY_H

/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#ifndef ARMARX_RELATIONCALCULATORCONTEXT_H
#define ARMARX_RELATIONCALCULATORCONTEXT_H

#include <boost/shared_ptr.hpp>
#include <MemoryX/interface/memorytypes/MemoryEntities.h>
#include <MMM/Motion/Motion.h>
#include <ArmarXCore/core/logging/Logging.h>

#include <VirtualRobot/CollisionDetection/CDManager.h>
#include <VirtualRobot/Robot.h>
#include <RobotAPI/libraries/core/FramedPose.h>



namespace armarx {

    class RelationCalculatorContext : Logging
    {
    public:
        RelationCalculatorContext();

        struct MotionData
        {
            MMM::MotionPtr motion;
            VirtualRobot::RobotPtr model;
        };
        struct ObjectData : MotionData
        {
            VirtualRobot::SceneObjectSetPtr sos;
            memoryx::ObjectClassBasePtr objectClass;
        };
        typedef std::map<std::string, MotionData> MotionDataMap;

        const ObjectData& getPrimaryObject(){ return primaryObject;}
        const ObjectData& getSecondaryObject(){ return secondaryObject;}

        VirtualRobot::CDManagerPtr getCDMan(){return cdMan;}
        FramedPosePtr getAgentPose(){return agentPose;}
        const MotionDataMap& getMotions(){return motions;}
        VirtualRobot::SceneObjectSetPtr getSceneObjectSet(const std::string &objectName);
        VirtualRobot::RobotPtr getModel(const std::string &objectName);

    protected:
        unsigned int frame;
        FramedPosePtr agentPose;
        MotionDataMap motions;
        VirtualRobot::CDManagerPtr cdMan;
        ObjectData primaryObject;
        ObjectData secondaryObject;
    };
    typedef boost::shared_ptr<RelationCalculatorContext> RelationCalculatorContextPtr;
}

#endif

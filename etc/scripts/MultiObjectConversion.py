#!/usr/bin/env python
import readline, glob, subprocess, re, os, sys, multiprocessing, errno, argparse

__author__ = 'waechter'

def get_object_name_from_path(path):
    # print path
    exp = re.compile("ConverterVicon2MMM_(?P<value>[a-zA-Z0-9_]+)Config.xml")
    m = exp.search(path)
    if m is not None:
        return m.group('value')
    return ""


def run_converter(converterConfigPath, motionFilePath,
                 converterType, modelsPath, converterApp):
    motions = []
    if os.path.isfile(motionFilePath):
        print "Single file: " + motionFilePath
        motions.append(motionFilePath)
    else:
        motions = glob.glob(motionFilePath+ "/*.c3d")
        print "Batch mode: "
        print motions
    filelist = os.listdir(converterConfigPath)
    print filelist
    for motion in motions:
        filesplit = os.path.splitext(motion)
        outputFilePath = filesplit[0] +".xml"
        if os.path.exists(outputFilePath) and len(outputFilePath) > 0:
            os.remove(outputFilePath)

        for curFile in filelist:

            fullFilePath = os.path.join(converterConfigPath, curFile)
            if not os.path.isfile(fullFilePath):
                continue


            cmd = converterApp + " --inputDataVicon " + motion + " --converter=" + converterType
            cmd += " --converterConfigFile " + fullFilePath
            cmd += " --outputModelProcessor \"\" --outputFile " + outputFilePath

            objectName = get_object_name_from_path(fullFilePath)
            if len(objectName) == 0:
                print "Object name empty for filepath: " + fullFilePath
                continue

            modelPath = os.path.join(modelsPath, objectName, objectName+".xml")
            cmd += " --outputModel " + modelPath
            print cmd
            os.system(cmd)
        #os.system("MMMViewer --motion " + outputFilePath)


parser = argparse.ArgumentParser(description='Multi Object Vicon->MMM converter')
parser.add_argument('--converterApp', help='Path to the converter app', default="MMMConverter", dest="converterApp")
parser.add_argument('--converters', "-c", help='Path to the converters', dest="converterPath", required=True)
parser.add_argument('--modelpath', "-M", help='Path to the model file', dest="modelsPath", required=True)
parser.add_argument('--convertertype', "-C", help='Type of the converter', dest="converterType",
                    default="RigidBodyConverterVicon2MMM")
parser.add_argument('--motionfile', "-m", dest='motionfile', help='Path of the c3d motionfile to be converted',
                    required=True)


args = parser.parse_args()

run_converter(args.converterPath, args.motionfile, args.converterType, args.modelsPath, args.converterApp)
